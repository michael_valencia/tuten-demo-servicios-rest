<?php
namespace App;
class ZonaHoraria
{
    private $hora;
    private $zonahoraria;

    public function __construct($horaP,$zonahorariaP)
    {
        $this->hora = $horaP;
        $this->zonahoraria = $zonahorariaP;
    }

    public function validarHora()
    {
        return preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/", $this->hora);
    }    

    public function validateZonaHoraria()
    {
        return  intval($this->zonahoraria) < -11 || intval($this->zonahoraria) > 14;
    }    

    public function calcularUTC()
    {
        //Dividimos la hora en un arreglo
        $hora = explode(":",$this->hora);
        // Sumamos o restamos según la zona horaria dada
        $hora[0] = $hora[0] + ($this->zonahoraria * -1);
        // Validación si la da menor que 0
        if($hora[0] < 0)
            $hora[0] = 24 - $hora[0];
        // Validación si la da mayor de 23
        if($hora[0] > 23)
            $hora[0] = $hora[0] - 24;
        
        $hora[0] = strlen($hora[0]) == 1 ? "0" . $hora[0] : $hora[0];

        return join(':',$hora);
    }
}
