<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// Modelo
use App\ZonaHoraria;

class TutenDemo extends Controller
{
    //Función encargada de obtener la hora en formato UTC
    public function timeFormatUTC(Request $request)
    {
        //Se realiza la validación que se haya envíado los parametros requeridos
        $this->validate($request, [
            'dato1' => 'required',
            'dato2' => 'required'
        ]);

        $zonaHorariaModel = new ZonaHoraria($request->all()['dato1'],$request->all()['dato2']);

        //Validación hora enviada
        if(!$zonaHorariaModel->validarHora())
            return response()->json(['error' => 'Bad Request','msg' => 'Formato de la hora inconsistente'], 400);

        //Validación zona horario
        if($zonaHorariaModel->validateZonaHoraria())
            return response()->json(['error' => 'Bad Request','msg' => 'Zona horaria no existe'], 400);
        
        return response()->json(['response' => 
                [   //Convertirmos la hora a formato UTC
                    'time' => $zonaHorariaModel->calcularUTC(),
                    'timezone' => 'utc'
                ]], 200);        
        
    }
}
