<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TutenTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTutenUTC()
    {
        //Parametros a enviar
        $parameters = [
            'dato1' => '18:31:45',
            'dato2' => -3,
        ];


        //Ejecución de nuestra ruta
        $this->post('/tuten-demo/servicios-rest',$parameters,[]);

        //Después de realizar la solicitud, comprueba si la solicitud devuelve el código de estado esperado
        $this->seeStatusCode(200);

        // Comprueba si los datos devueltos de nuestra solicitud siguen el esperado
        $this->seeJsonStructure(
            ['response' => 
                [
                  'time',
                  'timezone'
                ]
            ]
       );

       return 'Todo bien';
    }
}
