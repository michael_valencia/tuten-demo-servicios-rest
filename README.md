# Componentes utilizados - Tuten Demo Servicios Rest

Se ha uso de Lumen 5.7, este es un microframework de Laravel 5.7, el cual esta enfocado a la creación de micro-servicios y APIs

## Servidor de desarrollo

Ejecutar `php -S localhost:8000 -t public` en la consola de comandos.

## Estructura del proyecto

Creación de rutas de acceso - routes/web.php
Validación de datos de parámetros
Creación del controllador encargado de manejar la lógica - app/Http/Controllers/TutenDemo.php
Creación del modelo - app/ZonaHoraria.php
Response en formato JSON
Pruebas unitarias - tests/TutenTest.php
Ejecutar pruebas unitarias: phpunit --filter TutenTest

## Componentes necesario para iniciar el proyecto

Composer: https://getcomposer.org
PHP >= 7.1.3
Extensión activa: OpenSSL
Extensión activa: PDO
Extensión activa: Mbstring 
PHPUnit - Utilizado para verificar las pruebas unitarias
Cuando se clone el proyecto ejecutar composer update, para instalar las dependencias de Lumen/Laravel

## Link del servidor donde esta alojado el servicio

http://tuten-demo-servicio-rest.adp-solutions.com/tuten-demo/servicios-rest

Parametros
dato1 : 17:31:45 - type:text
dato2 : -3 - type:text

Header
Content-Type : application/json
Accept : application/json

## Link del cliente Postman

